
################
  Output Nodes
################

These nodes serve as outputs for node-based textures.

.. toctree::
   :maxdepth: 1

   output.rst
   viewer.rst
